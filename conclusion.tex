\chapter{Conclusion}\label{chp:conclusion}

\section{Thesis Summary}
In this thesis, we explored the mechanistic processes of task allocation in social insects on worker-life timescales from different novel perspectives. 
Particularly, the viewpoints that we focused on are not well-studied in the literature (reviewed in Chapter~\ref{chp:review}). 
Following a bottom-up approach based on timescales, we built theoretical models to explain and simulate the interplay between insect colonies and their environments. 
Based on these models, we analysed and discussed the colony-level efficiency as well as the flexibility in dynamic environments. 
Here we divided the factors that can influence task allocation into two categories: within-worker causes and between-worker interactions and approached each of them separately.
\\
\\
We investigated the effect of within-worker factors by taking account of the influence of time, which tends to be ignored by most studies (see Chapter~\ref{chp:model-internal}). 
Accordingly, we performed survival analysis for the data from experiments on the fanning task in bumblebees. 
Particularly, these experiments adopt a novel set-up which makes it possible that the environmental condition is measured and controlled precisely. 
Surprisingly, our results suggest that neither stimulus intensity nor individual experience strongly influence workers' task engagement. 
This is in contrast with most established empirical studies~\cite{ODonnell:2001iu,wkt-2002,Weidenmuller:2004ht,Westhus:2013bc} and theoretical models~\cite{btd-1996,Theraulaz:1998dx,Gautrais:2002jl,Lichocki:2012tk,Duarte:2012cw}, which do not account for the temporal influence.  
Moreover, our survival analysis suggests that workers tend to be less active after receiving rewards of fanning, which cannot be identified by traditional analysis. 
This contrasts with the general expectation that workers' perception of rewards should encourage them to engage in the task at a higher level.
\\
\\
We find that the processes of workers' fanning in bumblebees are not homogeneous Poisson but close to power laws, indicating the timing-patterns of workers' activities indeed cannot be omitted at short timescales. 
Consequently, we built a time-resolved model of task allocation, which can be verified with the empirical data and also used to explain the data. 
Based on this model, we illustrated that the colony-level efficiency cannot only be measured by how well the task-related demand is satisfied but also by how much workforce is available to other tasks (see Chapter~\ref{chp:efficiency-adaptation}). 
When the environment tends to fluctuate, how well a colony can adapt to environmental changes seems to depend on both the focal timescale and the balance between the rates of individual learning and the characteristics of environmental dynamics.
\\
\\
We applied game theory as a basic framework to explore the influence of social interaction on task allocation (see Chapter~\ref{chp:model-external}). 
Game theory provides a mathematical foundation and a modelling perspective to study social interactions, which can guide empirical work as the interactions between workers in a colony involve complex processes and are difficult to measure and control in biological experiments. 
Based on our game-theoretical models, we find that specialisation can emerge from the interaction dynamics between workers and their environments alone. 
This offers a new way of understanding the question -- What are the primary sources that can cause variation in workers' task preference, which is one of the core interests in the study of task allocation~\cite{Gordon:2015jd}. 
Most studies regard inherent inter-individual differentiation as the main cause~\cite{Jeanson:2013bs}, while it is also shown that task specialisation can result from a colony of identical workers with experience-based reinforcement~\cite{Theraulaz:1998dx} or spatial variation of localised task demands~\cite{Tofts:1992ig,Johnson:2010gc}. 
Our results suggest that an alternative source of variation in task preference can be social interactions between workers.
\\
\\
We find that variation of environmental conditions and mechanisms that determine the dynamics of workers' strategies over time can cause different behavioural patterns and efficiency achievements of task allocation. 
Our game-theoretical models can be used to explore the dynamics of cooperative games and illustrated the principle ``Tragedy of the Commune''~\cite{Doebeli:2004fp}. 
For dynamic environments, our simulations suggest that surprisingly, the processes of task allocation may exhibit an effect of hysteresis as the behavioural patterns depend on the previous history of environmental conditions (see Chapter~\ref{chp:efficiency-adaptation}). 
This provides new insights into our understanding of how the processes of task allocation can adapt to environmental fluctuations, in addition to the traditional assumption that colonies are highly responsive to the current environmental conditions.

\section{Further Research}

\subsection{Empirical Studies}
Our time-resolved model of task allocation conceptually captures multiple tasks in an insect colony (see Figure~\ref{fig:time-resolved-model-conceptual}). 
It was reduced to only two tasks for comparison and verification with the data from the experiments in which, as a first step, only one controlled task was set, due to practical difficulties. 
Further empirical studies could test our time-resolved model by setting up experiments with an additional controlled task, such as foraging. 
The reason behind the negative influence of individual efficiency on workers' fanning engagement is likely to be that fanning is a homeostatic task, the stimulus of which needs to be maintained within a certain range rather than reduced at the lowest possible level (see Section~\ref{sec:internal-discussion}). 
In addition, by the experiments with a controlled task of foraging, we can verify if the general expectation that the perception of rewards of performing a task tends to encourage workers to engage in the task more strongly holds for a maximising task.
\\
\\
The results of our survival analysis suggest that individual experience does not strongly influence workers' task engagement on the moment-to-moment timescale. 
It would be interesting to empirically explore the influence of individual experience on task engagement at an extended timescale, for example, over a few days~(Anja Weidenm{\"u}ller, personal communication). 
Apart from individual experience, future work can investigate the influence of social experience on workers' task engagement by extending the experimental set-up (see Section~\ref{sec:internal-experiment}) to multiple workers and brood dummies within one test arena~(Linda Garrison and Anja Weidenm{\"u}ller, experiments ongoing). 
The experiments in which multiple workers and brood dummies are set in a single test arena may also be used to test the hypotheses about the influence of social interaction on task allocation, for instance, the predictions from our game-theoretical models (see Chapter~\ref{chp:model-external}).

\subsection{Theoretical Approaches}
Our task-allocation game (introduced in Section~\ref{sec:task-allocation-game}) can be integrated with a variety of learning mechanisms that determine the dynamics of workers' strategies over time.  
Particularly, the model with task recruitment (see Section~\ref{sec:model-task-recruitment}), potentially has much richer ecological details to further explore. 
The outcomes of our game-theoretical models are based on a game of allocation between a homeostatic task and a maximising task. 
Future research may investigate how the results can vary if a different type of task is introduced into this task-allocation game. 
One potential option could be inactivity, which is widely observed to take a large proportion of task repertoire in social insects~\cite{Dornhaus:2015ew}. 
The results may contribute to our understanding of the functional roles of the presence of inactive workers in a colony.
\\
\\
The question of how colonies can adapt to dynamic environments needs to be further studied. 
For the influence of within-worker factors, future work may explore whether environment-dependent rates of workers' reinforcement by individual experience can lead to a trade-off between efficiency and flexibility; 
For the influence of interactions between workers, further research could systematically investigate how fast colonies with different behavioural patterns can adapt to environmental fluctuations.
\\
\\
Overall, the outcomes of this research contribute to the knowledge of how the colony-level patterns of task allocation can emerge from individual task choice in social insects. 
Our methodology demonstrates a set of approaches for modelling in an interdisciplinary study.  
For the influence of within-worker factors, we regard task allocation as time-dependent stochastic processes and explore temporal dynamics, facilitating a fine-grained analysis of the empirical data and consequently improves our understanding of the underlying mechanisms. 
How workers interact with others and their environments is one of the key questions in the study of task allocation~\cite{Gordon:2015jd}. 
The modelling framework based on game theory opens numerous opportunities to explore the dynamics of social interaction between workers in a variety of ecological contexts. 
The game-theoretical models provide a new self-organisational perspective for how task allocation can vary with environmental conditions, as an alternative to the response-threshold models which are widely regarded as the main paradigm in the literature (reviewed in Section~\ref{sec:review-response-threshold}). 
Our models can be used to explain and predict the behavioural patterns of task allocation and to guide further biological experiments. 
Our results may ultimately benefit the bio-inspired applications such as swarm robotic systems~\cite{Krieger:2000fl,Zhang:2007ez} and multi-task scheduling in factories~\cite{bstd-1997,cbtd-2000,Cicirello:2004ke}.