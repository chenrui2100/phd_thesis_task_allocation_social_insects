\chapter{Introduction}\label{chp:introduction}

\section{Task Allocation in Social Insects}\label{sec:introduction-topic}
Social insects such as ants, bees, wasps and termites live in groups, typically called colonies, of up to millions of individuals. 
A colony includes one or few queens, and sometimes males, both of which are responsible for reproduction. 
The majority of individuals in a colony are workers, taking charge of the maintenance and expansion of the colony. 
Social insect colonies are regarded as one of the most successful organisations in nature~\cite{hw-1990,Holldobler:2009ug,Charbonneau:2013wa,Oster:1978tf,Grimaldi:2005wx}. 
They dominate most terrestrial habitats around the world~\cite{Charbonneau:2013wa,Dornhaus:2015ew,Holldobler:2009ug} and some species may have evolved for millions of years~\cite{Grimaldi:2005wx}.
\\
\\
The collective behaviours of social insect colonies, adapted to a variety of environmental and social conditions, are complex and diverse~\cite{Dornhaus:2012gt,Dornhaus:2015ew} and thus provide a rich field to investigate. 
Within such a colony, individual members appear to act in a decentralised way and make simple decisions according to their limited local knowledge~\cite{Bonabeau:1997tq,cdfstb-2001,Duarte:2011wu,Couzin:2009iq}. 
This makes social insect colonies experimentally more tractable than many other collective organisations and as a result, they are widely used as model systems for the study of collective behaviour~\cite{bf-2001,Dornhaus:2015ew}. 
The principles of collective behaviour are potentially transferable from social insect colonies to other social systems or even multicellular organisms~\cite{bf-2001,Duarte:2011wu,Wilson:1985je}.
\\
\\
Workers within a colony can cooperate in sophisticated ways for various tasks such as brood care, nest construction, foraging and defence in order to satisfy the colony needs that cannot be managed by a single individual~\citep{Charbonneau:2013wa,Dornhaus:2015ew,Duarte:2011wu,Mersch:2016hp}. 
Without any central control, workers can be appropriately allocated to each task in response to numerous environmental contexts such as variation of food resources, predation pressures and climatic conditions~\cite{Duarte:2011wu,GORDON:1996ud,Oster:1978tf,Robinson:1992dm}. 
This process is called \textit{task allocation}~\cite{GORDON:1996ud,GORDON:2002ud,Gordon:2015jd}. 
It is fundamental to the organisation of insect colonies and crucial to their survival and ecological success~\cite{bf-2001,Jeanson:2013bs,Oster:1978tf,Schwander:2005eq,Wilson:1971tn,Wilson:1985je,PageJr:1990kd}. 
Task allocation is one of the core questions in the study of social insects and connected with a wide range of areas including behavioural syndromes~\cite{Jandt:2014ck}, scaling laws~\cite{Fewell:2016if} and network dynamics~\cite{Charbonneau:2013wa,Mersch:2016hp}. 
\\
\\
Task allocation is a fast and highly dynamic process that describes how distributions of colony workforce across different tasks interact with environments~\cite{Oster:1978tf,Gordon:2015jd,Fewell:2016if,Mersch:2016hp,Theraulaz:2016fe}. 
Over a certain time period, workers can behave with statistically consistent preferences towards particular tasks, which is called \textit{task specialisation}~\cite{Duarte:2011wu,Fewell:2016if,Jeanne:2016da}. 
To satisfy various demands within a colony, workers can specialise in different subsets of tasks, called \textit{division of labour}~\cite{Michener:1974tu,Oster:1978tf,Robinson:1992dm,Fewell:2016if,Jeanne:2016da}. 
Here a stable, long-term division of labour is out of scope of this thesis. 
This research mainly focuses on the dynamics of task allocation on short worker-life timescales.
\\
\\
Particularly, specialisation can be used to describe the behavioural patterns of task allocation. 
For example, there are 100 workers who need to tackle two tasks ($A$ and $B$) in a colony. 
In order to satisfy the associated demands, Task $A$ requires 30 workers' engagement and Task $B$ needs 70. 
At the colony level, one pattern of task allocation is that all workers are engaged in both tasks with 30\% of chance for Task $A$ and 70\% for Task $B$, which does not involve specialisation at all; 
Another pattern is that 30 workers are allocated to Task $A$, 70 to Task $B$ and all workers make 100\% effort at their target tasks, which represents a complete colony-level specialisation; 
There can be other patterns of task allocation in between the above two.
\\
\\
Although there is little evidence that workers permanently specialise into tasks~\cite{Gordon:2015jd}, the colony-level specialisation can statistically occur on certain timescales~\cite{Duarte:2011wu,Jeanne:2016da}.
In the study of task allocation, there are some major questions such as what are the sources that can generate specialisation~\cite{Gordon:2015jd} and how specialisation is related to behavioural efficiency and flexibility~\cite{Dornhaus:2015ew}. 
At the colony level, it is assumed that tasks can be performed more efficiently by specialised workers~\cite{Oster:1978tf,Dornhaus:2015ew,Leighton:2016ic}. 
We will explore and discuss these questions in the following chapters of this thesis.

\section{Research Questions}\label{sec:introduction-questions}
To study any animal behaviour, the most fundamental and significant approach is to ask \textit{Tinbergen's four questions}~\cite{Tinbergen:1963kn,Laland:2013is}: causation (How does it work?), ontogeny (How did it develop?), adaptive value (What is it for?) and evolution (How did it evolve?). 
These questions provide complimentary insights and lead to a comprehensive understanding of the focal behaviour~\cite{Laland:2013is}. 
Among these questions, we focus on the underlying mechanisms of task allocation in social insects on short worker-life timescales, which are connected with causation and ontogeny, as well as the associated efficiency achievement, which is related to the adaptive value of task allocation. 
Here the evolutionary dynamics of task allocation are out of scope of this thesis. 
We propose the following questions for this research:
\begin{description}
	\item[1)] What are the underlying factors that can influence task allocation in social insects? (causation)
	\item[2)] What mechanisms potentially determine the dynamics of workers' strategies for task allocation over time in social insects? (ontogeny)
	\item[3)] How can environmental conditions affect task allocation in social insects? (causation)
	\item[4)] How well can social insect colonies perform in terms of task allocation? (adaptive value)
	\item[5)] How and how well can task allocation in social insects adapt to dynamically changing environments? (ontogeny \& adaptive value)
\end{description} 

\section{Methodology}
Task allocation in social insects exhibits a \textit{self-organised} process which emerges from interactions of workers who follow simple behavioural rules in response to environmental conditions~\cite{Bonabeau:1997tq,Robinson:1992dm,Duarte:2011wu,PageJr:1998hd,Gordon:2015jd,Theraulaz:2016fe}. 
We divide the behavioural rules that potentially determine workers' task allocation into individual and social: Individual behavioural rules represent the factors within a worker; Social behavioural rules refer to the interactions between workers. 
For individual behavioural rules, we perform survival analysis on the data from experiments in bumblebees (see Chapter~\ref{chp:model-internal}). 
Based on this analysis, we explore the influence of potential factors on workers' task selection and propose a novel model which is then verified with the empirical data.  
To explore the effect of social interaction, we use game theory as a basic framework (see Chapter~\ref{chp:model-external}). 
We integrate a task-allocation game with different mechanisms of how workers' strategies for task allocation can develop over time.
Then we build computer simulations based on these integrated models with a range of environmental conditions. 
Our models for both individual and social behavioural rules are simulated for the analysis of efficiency and flexibility in dynamic environments (see Chapter~\ref{chp:efficiency-adaptation}).

\section{Key Contributions}

\subsection{Knowledge}
The outcomes of this research make the following contributions to the knowledge of task allocation in social insects: 
\begin{itemize}
	\item Neither stimulus intensity nor individual experience has a significant effect on the moment-to-moment task allocation in bumblebee thermoregulation under certain environmental conditions;
	\item Bumblebee workers tend to be less active after receiving rewards from a homeostatic task on the moment-to-moment timescale;
	\item The timing-patterns of workers' activities in bumblebees are close to power laws rather than homogeneous Poisson on the moment-to-moment timescale;
	\item Specialisation can emerge from the interaction dynamics between workers alone under certain environmental conditions;
	\item Variation of environmental conditions and mechanisms that determine the dynamics of workers' strategies over time can lead to different behavioural patterns and efficiency achievements of task allocation;
	\item The history of previous environmental conditions can influence how social insect colonies adapt to dynamic environments.
\end{itemize}

\subsection{Methodology}
The results of this study make the following contributions to the methodology of task allocation in social insects:
\begin{itemize}
	\item Exploring task allocation of social insect colonies in a bottom-up approach based on timescales;
	\item Conducting survival analysis for the influence of within-worker factors on task allocation in social insects;
	\item Using game theory as a basic framework to study the effect of social interaction on task allocation in social insects.
\end{itemize}

\section{Thesis Outline}
In Chapter~\ref{chp:review}, we review current theoretical models of task allocation as well as empirical studies in the literature. 
Based on this, we identify the limitations of these models and accordingly make suggestions for future research directions. 
In Chapter~\ref{chp:model-internal}, we explore the influence of potential factors within an individual worker based on the results of survival analysis for experimental data in the thermoregulation of bumblebees. 
Accordingly, we construct an agent-based model which explicitly takes the temporal effect into account. 
In Chapter~\ref{chp:model-external}, we apply game theory as a basic framework to study the impact of social interaction between workers on task allocation. 
We build computer simulations based on different mechanisms of how workers' strategies for task allocation can develop over time with a range of environmental contexts. 
In Chapter~\ref{chp:efficiency-adaptation}, we analyse the efficiency of task allocation based on our models introduced in the previous chapters and discuss the flexibility in dynamic environments with illustration by computer simulations.